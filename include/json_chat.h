//
// Created by 周圳元 on 2023/3/29.
//

#ifndef WEWAYCHAT_JSON_H
#define WEWAYCHAT_JSON_H
#include "server.h"
#include "user.h"
#include <json/json.h>

/**
 * 将json字符串解析成user结构体
 * @return 解析成功返回1，失败返回0
 */
int json_to_user(const char *json,user_t *user);

/**
 * 从大json字符串中根据key值取一个字符串类型的数据
 * @param json json字符串
 * @param key key值
 * @return 字符串 失败返回NULL
 */
char *json_get_string(const char *json,const char *key);

/**
 * 从大json字符串中根据key值取一个整型类型的数据
 * @param json json字符串
 * @param key key值
 * @return 取一个整型数据
 */
int json_get_int(const char *json,const char *key);

/**
 * 将用户结构体转化为json字符串
 * @return user的json字符串 失败返回NULL
 */
char* user_to_json(user_t user);

/**
 * 交互消息通用结构
 * @param action 用户/系统 的操作
 * @param data 发送的数据
 * @param text 一般文本
 * @return 返回message_json字符串，失败返回NULL
 */
char * message_init(int action,char *data,char *text);

/**
 * 从json中拿long值，但是该long只能是字符串
 * @param json json
 * @param key key值
 * @return 获得long值
 */
long json_get_long(const char *json,const char *key);

/**
 * 创建一个json或者往json中存放一个值
 * @param json NULL 创建json
 * @param key
 * @param data
 * @return
 */
char *json_set_int(char *json,char *key,int data);

char *json_set_char(char *json,char *key,char *data);



char * array_to_json(char *array[],int len);

// 获得长度和字符串数组
int json_to_array(char *arrayRet[], char *json, int len);

#endif //WEWAYCHAT_JSON_H
