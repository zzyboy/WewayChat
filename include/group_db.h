//
// Created by 周圳元 on 2023/3/30.
//

#ifndef WEWAYCHAT_GROUP_DB_H
#define WEWAYCHAT_GROUP_DB_H
#include <sqlite3.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
//#define DATABASE_PATH "/Users/zhouzhenyuan/CLionProjects/WeWayChat/server/db/server.db"
#define DATABASE_PATH "/home/user/shared/chart.db"

typedef struct {
    int group_id;
    char group_name[50];
    int creator_id;
    char creator_name[50];
    int create_time;
    char remark[200];
} group_t;

/**
 * 插入组
 * @param group 组结构体
 * @return 插入成功返回最新的id，插入失败返回-1
 */
int insert_group(group_t group);

int update_group(group_t group);

int delete_group(int groupId);
// flag是否倒序，0否 1是
group_t* select_group(group_t group,int *len,int flag);


/**
 * 获取加入的群聊信息
 */
int get_join_groups(int user_id,group_t *group);
#endif //WEWAYCHAT_GROUP_DB_H
