//
// Created by 周圳元 on 2023/3/29.
//

#ifndef WEWAYCHAT_SERVER_H
#define WEWAYCHAT_SERVER_H


#define INT_PORT 8080 // 默认服务端端口
#define INT_IP_ADDR "127.0.0.1" //默认IP
#define MAX_CLIENTS 40 //最大客户端连接数
#define BUFFER_SIZE 4096 //buf大小
#define JSON_SIZE_MAX 2048 // json字符串最大大小

// 头文件
#include <stdio.h>
#include <errno.h>
#include <netdb.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sqlite3.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <stddef.h>

#define NEW_INT (int *)(malloc(sizeof(int))) //给int分配空间
#define NEW_LONG (long *)(malloc(sizeof(long)))

typedef struct {
    int user_action; // 用户操作
    char* data; //传过来的json字符串 数据
    char* msg; //通用信息
} message_t;

/**
 * 服务器核心，启动socket并开启监听
 * @param port 启动端口
 * @return 启动成功返回socket 描述符，失败返回0并打印错误原因
 */
int core(int port,const char *ip_addr);

/**
 * 服务器启动，并多线程访问，将用户的socket存放到全局变量中
 * @param port 端口号
 * @param ip_addr IP地址
 * @return 启动成功返回1，失败返回0
 */
int socket_bind(int port,const char *ip_addr);


#endif //WEWAYCHAT_SERVER_H
