//
// Created by 周圳元 on 2023/3/29.
//
#include "server.h"
#ifndef WEWAYCHAT_TEST_H
#define WEWAYCHAT_TEST_H

// 用户操作
#define USER_LOGIN 1001 // 用户登录操作
#define USER_REGISTER 1002 // 用户注册
#define USER_FIND_ONLINE 1003 //查看在线好友
#define USER_FIND_F_ALL 1004 // 查看所有好友
#define USER_FIND_F_BY_NAME 1005 // 根据用户名查看自己的好友
#define USER_FIND_BY_NAME 1006 //根据用户名搜索用户
#define USER_ADD_F 1007 // 用户添加好友
#define USER_DELETE_F 1008 // 用户删除好友
#define USER_CRT_GROUP 1101 // 创建群聊
#define USER_JOIN_GROUP 1102 //加入群聊
#define GROUP_DELETE_USER 1103 // 群主踢用户
#define USER_GET_GROUP 1104 //获取群聊

#define USER_SEND_MESSAGE_USER 1200 // 用户私聊
#define USER_SEND_MESSAGE_GROUP 1201 // 用户发给群组消息
#define USER_SEND_MESSAGE_USER_OFF 1202 //用户不在线
#define USER_OUT_ONLINE 1999 // 用户退网（注销）

#define USER_GROUP_SEND_NO_FRIEND 4009// 请先添加群聊好友
#define SERVER_SEND_ALL 4100 // 系统广播
#define SERVER_ERROR 4000 // 服务器错误
#define LOGIN_ERROR 4001 //登录失败
#define REGISTER_ERROR 4002 // 注册失败

#define NORMAL_ERROR 3999 // 一般错误

#define SUCCESS 2001//操作成功

// 用户结构体
typedef struct {
    int	user_id; //用户id
    char user_name[50]; //用户名
    char passwd[50]; //用户密码
    char create_time[50]; //用户创建时间
} user_t;

typedef struct {
    int client;
    user_t user;
} client_t;

#define NEW_USER (user_t *)(malloc(sizeof(user_t)))

/**
 * 用户登录操作
 * @param username 用户名
 * @param password 密码
 * @return 1登录成功 0密码错误 -1 不存在用户
 */
int login(char *username,char *password);

/**
 *  用户注册
 * @param username 填充的用户名
 * @param password 填充密码
 * @return 0注册成功,-1有重复的用户名
 */
int registe(char *username,char *password);

/**
 *
 * @param action
 * @param req 请求json
 * @param rep
 * @param len 是客户端长度
 * @return
 */
int user_handle(int action,char *req,char *rep,client_t client[],int num,int len);


#endif //WEWAYCHAT_TEST_H
