#include "../client_core/client_us.h"

//#include <ncurses.h>
//#include <locale.h>
#define OPEN_WINDOW 10 //用户打开初始窗口
#define LOGIN_VIEW 11 //用户正在登录窗口
#define REG_VIEW 12 // 用户正在注册窗口
#define EXIT_VIEW 13 // 用户退出
#define HOME_VIEW 14 // 用户主页

int ch;
char ip[20];
int port =0;
int viewStats = 10;
char username[50];
char password[50];
int userId;
int winref = 1;
int cur = 0;
user_t user;
int socketfd;
int key_init = 0; // 选择位置
int selecur = 0; // 选择状态
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void lock_do(int stats) {
    pthread_mutex_lock(&mutex);
    viewStats = stats;
    pthread_mutex_unlock(&mutex);
}

void lock_ch_do(char t) {
    pthread_mutex_lock(&mutex);
    ch = t;
    pthread_mutex_unlock(&mutex);
}

//void logo_view() {
//    int top = 5;
//    int left = 50;
//    long sec = 100 * 600;
//    wattron(stdscr,COLOR_BLUE);
//    mvwprintw(stdscr,top ++,left," ▄         ▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄         ▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄         ▄       ▄▄▄▄▄▄▄▄▄▄▄  ▄         ▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄ ");
//    usleep(sec);
//    refresh();
//    mvwprintw(stdscr,top ++,left,"▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░▌       ▐░▌     ▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌");
//    usleep(sec);
//    refresh();
//    mvwprintw(stdscr,top ++,left,"▐░▌       ▐░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░▌       ▐░▌▐░█▀▀▀▀▀▀▀█░▌▐░▌       ▐░▌     ▐░█▀▀▀▀▀▀▀▀▀ ▐░▌       ▐░▌▐░█▀▀▀▀▀▀▀█░▌ ▀▀▀▀█░█▀▀▀▀ ");
//    usleep(sec);
//    refresh();
//    mvwprintw(stdscr,top ++,left,"▐░▌       ▐░▌▐░▌          ▐░▌       ▐░▌▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌          ▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ");
//    usleep(sec);
//    refresh();
//    mvwprintw(stdscr,top ++,left,"▐░▌   ▄   ▐░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░▌   ▄   ▐░▌▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄█░▌     ▐░▌          ▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄█░▌     ▐░▌     ");
//    usleep(sec);
//    refresh();
//    mvwprintw(stdscr,top ++,left,"▐░▌  ▐░▌  ▐░▌▐░░░░░░░░░░░▌▐░▌  ▐░▌  ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌     ▐░▌          ▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌     ▐░▌     ");
//    usleep(sec);
//    refresh();
//    mvwprintw(stdscr,top ++,left,"▐░▌ ▐░▌░▌ ▐░▌▐░█▀▀▀▀▀▀▀▀▀ ▐░▌ ▐░▌░▌ ▐░▌▐░█▀▀▀▀▀▀▀█░▌ ▀▀▀▀█░█▀▀▀▀      ▐░▌          ▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀█░▌     ▐░▌     ");
//    usleep(sec);
//    refresh();
//    mvwprintw(stdscr,top ++,left,"▐░▌▐░▌ ▐░▌▐░▌▐░▌          ▐░▌▐░▌ ▐░▌▐░▌▐░▌       ▐░▌     ▐░▌          ▐░▌          ▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ");
//    usleep(sec);
//    refresh();
//    mvwprintw(stdscr,top ++,left,"▐░▌░▌   ▐░▐░▌▐░█▄▄▄▄▄▄▄▄▄ ▐░▌░▌   ▐░▐░▌▐░▌       ▐░▌     ▐░▌          ▐░█▄▄▄▄▄▄▄▄▄ ▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ");
//    usleep(sec);
//    refresh();
//    mvwprintw(stdscr,top ++,left,"▐░░▌     ▐░░▌▐░░░░░░░░░░░▌▐░░▌     ▐░░▌▐░▌       ▐░▌     ▐░▌          ▐░░░░░░░░░░░▌▐░▌       ▐░▌▐░▌       ▐░▌     ▐░▌     ");
//    usleep(sec);
//    refresh();
//    mvwprintw(stdscr,top,left," ▀▀       ▀▀  ▀▀▀▀▀▀▀▀▀▀▀  ▀▀       ▀▀  ▀         ▀       ▀            ▀▀▀▀▀▀▀▀▀▀▀  ▀         ▀  ▀         ▀       ▀      ");
//    usleep(sec);
//    refresh();
//    wattroff(stdscr,COLOR_BLUE);
//}



// 显示界面线程
void *view(void *arg) {
//    start_color();
//
//    WINDOW *win = newwin(25, 80, (LINES - 25) / 2, (COLS - 80) / 2);
//    noecho();// 关闭光标
//    cbreak(); // 允许退出
//    int refsh = 1;
//    wattron(stdscr,COLOR_BLUE);
//    wbkgd(stdscr,COLOR_BLUE);
//    refresh();
//    keypad(stdscr,TRUE);
//    logo_view();
//    viewStats = OPEN_WINDOW; // 用户刚打开窗口
//    int cur_potx = 0,cur_poty = 0;//光标位置
//    sleep(1);
//    while(1) {
//
//        switch (viewStats) {
//            case OPEN_WINDOW: {
//                WINDOW *win = newwin(25, 80, (LINES - 25) / 2, (COLS - 80) / 2);
//                keypad(win, TRUE);
//                keypad(stdscr,TRUE);
//                attron(A_NORMAL);
//                attroff(COLOR_PAIR(1));
//                box(win,0,0); // 边框定义
//                init_pair(1, COLOR_WHITE, COLOR_MAGENTA);
//                init_pair(2, COLOR_BLUE, COLOR_MAGENTA);
//                init_pair(3, COLOR_RED, COLOR_BLUE);
//                wbkgd(win, COLOR_PAIR(1));
//                wattron(win,COLOR_PAIR(2));
//                mvwprintw(win, 1, 35, "登录页面");  // 添加登录
//                wattroff(win,COLOR_PAIR(2));
//                if (key_init == 0) {
//                    wattron(win,COLOR_PAIR(3));
//                    mvwprintw(win, 16, 25,"[1]注册");
//                    wattroff(win,COLOR_PAIR(3));
//                    winref = 1;
//                } else {
//                    mvwprintw(win, 16, 25,"[1]注册");
//                }
//                if (key_init == 3) {
//                    wattron(win,COLOR_PAIR(3));
//                    mvwprintw(win, 16, 39,"[2]登录");
//                    wattroff(win,COLOR_PAIR(3));
//                    winref = 1;
//                } else {
//                    mvwprintw(win, 16, 39,"[2]登录");
//                }
//                if (key_init == 4) {
//                    wattron(win,COLOR_PAIR(3));
//                    mvwprintw(win, 16, 53,"[3]忘记密码?");
//                    wattroff(win,COLOR_PAIR(3));
//                    winref = 1;
//                } else {
//                    mvwprintw(win, 16, 53,"[3]忘记密码?");
//                }
//                if (selecur == 0) {
//                    curs_set(0);
//                }
//                if (selecur == 1) {
//                    if (key_init == 0) {
//                        curs_set(1);
//                        wrefresh(win);
//                        winref = 0;
//                    }
//                }
//                if (ch == ':') {
//                    move(30,30);
//                    printw("选择模式");
//                    cur = 1;
//                    curs_set(0);
//                    refresh();
//                    ch = '\0';
//                }
//                if (cur == 1) {
//                    if (ch == '1') {
//                        key_init = 0;
//                    }
//                    if (ch == '2') {
//                        key_init = 1;
//                    }
//                    if (ch == '3') {
//                        key_init = 2;
//                    }
//                    if (ch == '4') {
//                        key_init = 3;
//                    }
//                    if (ch == '5') {
//                        key_init = 4;
//                    }
//                    if (ch == '\n') {
//                        selecur = 1;
//                        move(30,30);
//                        printw("       ");
//                        refresh();
//                        cur = 0;
//                        curs_set(0);
//                        ch = '\0';
//                    }
//                    if (ch == 127) {
//                        key_init = 1;
//                    }
//                    move(34,36);
//                    printw("%d",ch);
//                }
//                if (refsh) {
//                    refresh();
//                    refsh --;
//                }
//                if (winref) {
//                    wrefresh(win);
//                    winref--;
//                }
//            }
//        }
//    }

   while (1) {
       switch (viewStats) {
           case OPEN_WINDOW: {
               switch (ch) {
                   case '\0': {
                      break;
                   }
                   case 'P': {
                       printf("1.登录 \n");
                       printf("2.注册 \n");
                       printf("3.退出 \n");
                       lock_ch_do('\0');
                       break;
                   }
                   case '1': {
                       printf("登录\n");
                       printf("请输入用户名和密码 \n");
                       lock_do(LOGIN_VIEW);
                       break;
                   }
                   case '2': {
                       printf("注册\n");
                       printf("请输入用户名和密码 \n");
                       printf("%c",ch);
                       lock_do(REG_VIEW);
                       break;
                   }
                   case '3': {
                       lock_do(EXIT_VIEW);
                       break;
                   }
               }
               break;
           }
           case HOME_VIEW: {
                switch(ch) {
                    case '\0': {
                        break;
                    }
                    case 'P': {
                        printf("这里是主页 \n");
                        lock_ch_do('\0');
                        break;
                    }
                }
           }
       }
   }
}

void *do_test(void *arg) {
    while (1) {
//        sleep(2);
//        pthread_mutex_lock(&mutex);
//        printf("%d \n",viewStats);
        switch (viewStats) {
            case OPEN_WINDOW: {
//                if (selecur) {
//                    ch = wgetch(stdscr);
//                } else {
//                ch = wgetch(stdscr);
//            }
                break;
            }
            case REG_VIEW: {
                scanf("%s %s",username,password);
                register_account(socketfd,username,password);
                printf("test");
                lock_do(OPEN_WINDOW);
                lock_ch_do('P');
                // 归位
                break;
            }
            case LOGIN_VIEW: {
                scanf("%s %s",username,password);

                // 假设登录成功，进入主页
                lock_do(HOME_VIEW);
                lock_ch_do('P');
                break;
            }
            case EXIT_VIEW: {
                exit(-1);
            }
        }
//        pthread_mutex_unlock(&mutex);
    }
}



int main(int argc,char **argv){
//    setlocale(LC_ALL,"");
//    initscr(); //初始化终端
//    start_color();


    if(argc < 3){
        printf("建议使用./client <IP> <PORT> 格式启动客户端 \n");
        printf("否则将使用./client 127.0.0.1 8080 默认启动客户端 \n");
        port = INT_PORT;
        strcpy(ip,INT_IP_ADDR);
    } else if (argc == 3) {
        port = atoi(argv[2]);
        strcpy(ip,argv[1]);
    } else {
        printf("建议使用./client <IP> <PORT> 格式启动客户端 \n");
        printf("参数不明 退出程序 \n");
        exit(-1);
    }

    pthread_t thread_id1;
    pthread_t thread_id2;
    if (pthread_create(&thread_id1, NULL, view, NULL) != 0) {
        printf("Error creating client thread\n");
        exit(-1);
    }
    if (pthread_create(&thread_id2, NULL, do_test, NULL) != 0) {
        printf("Error creating client thread\n");
        exit(-1);
    }

//    pthread_mutex_lock(&mutex);
    socketfd = regis_init(port,ip);

    int d = register_account(socketfd,"qeqwewqqe","qweqweq");
    printf("%d \n",d);
//    pthread_mutex_unlock(&mutex);

    while(1);
    return 0;
}