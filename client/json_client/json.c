//
// Created by 周圳元 on 2023/3/29.
//
#include "../../include/json_chat.h"

char *json_get_string(const char *json,const char *key) {
    struct json_object *jsonObject;
    jsonObject = json_tokener_parse(json);
    struct json_object *jsonObject1;
    jsonObject1 = json_object_object_get(jsonObject,key);
    return json_object_get_string(jsonObject1);
}

int json_get_int(const char *json,const char *key) {
    struct json_object *jsonObject;
    jsonObject = json_tokener_parse(json);
    struct json_object *jsonObject1;
    jsonObject1 = json_object_object_get(jsonObject,key);
    return json_object_get_int(jsonObject1);
}

long json_get_long(const char *json,const char *key) {
    struct json_object *jsonObject;
    jsonObject = json_tokener_parse(json);
    struct json_object *jsonObject1;
    jsonObject1 = json_object_object_get(jsonObject,key);
    return json_object_get_int(jsonObject1);
}

char* user_to_json(user_t user) {

    struct json_object *object = json_object_new_object();

    if (user.user_name[0] != '\0') {
        json_object_object_add(object,"user_name", json_object_new_string(user.user_name));
    }
    if (user.passwd[0] != '\0') {
        json_object_object_add(object,"passwd", json_object_new_string(user.passwd));
    }
    if (user.user_id != 0) {
        json_object_object_add(object,"user_id",json_object_new_int(user.user_id));
    }

    if (user.create_time[0] != '\0') {
        json_object_object_add(object,"create_time",json_object_new_int(user.create_time));
    }

    char* ob_str = json_object_to_json_string(object);

    return ob_str;
}

int json_to_user(const char *json,user_t *user) {
    strcpy(user->user_name, json_get_string(json,"user_name"));
    int *userId = user->user_id = NEW_INT;
    *userId = json_get_int(json,"user_id");
    strcpy(user->passwd, json_get_string(json,"passwd"));
    strcpy(user->create_time,json_get_string(json,"create_time"));
    return 1;
}

char * message_init(int action,char *data,char *text) {
    struct json_object *object = json_object_new_object();
    if (text != NULL) {
        json_object_object_add(object,"text", json_object_new_string(text));
    }
    if (data != NULL) {
        json_object_object_add(object,"data", json_object_new_string(data));
    }
    if (action != 0) {
        json_object_object_add(object,"action", json_object_new_int(action));
    }
    char* ob_str = json_object_to_json_string(object);
    return ob_str;
}

char *json_set_int(char *json,char *key,int data) {
    struct json_object *jsonObject;
    char *jsoret = json;
    if (json == NULL) {
        jsoret = "{}";
    }
    jsonObject = json_tokener_parse(jsoret);
    if (key == NULL) {
        return NULL;
    }
    json_object_object_add(jsonObject,key,json_object_new_int(data));
    char* ob_str = json_object_to_json_string(jsonObject);
    return ob_str;
}

char *json_set_char(char *json,char *key,char *data) {
    struct json_object *jsonObject;
    char *jsoret = json;
    if (json == NULL) {
        jsoret = "{}";
    }
    jsonObject = json_tokener_parse(jsoret);
    if (key == NULL || data == NULL) {
        return NULL;
    }
    json_object_object_add(jsonObject,key,json_object_new_string(data));

    char* ob_str = json_object_to_json_string(jsonObject);
    return ob_str;
}

char * array_to_json(char *array[],int len) {
    struct json_object *jsonObject = json_object_new_array();
    for (int i = len-1;i >= 0;i--) {
        struct json_object* json_string = json_tokener_parse(array[i]);
        json_object_array_add(jsonObject,json_string);
    }
    return json_object_to_json_string(jsonObject);
}

int json_to_array(char *arrayRet[], char *json, int len) {
    printf("json字符串是 %s",json);
    struct json_object *jsonObject = json_tokener_parse(json);
    int size = json_object_array_length(jsonObject);
    if (size > len) {
        // 数组大小不足，返回错误
        return -1;
    }
    for (int i = size - 1; i >= 0; i--) {
        struct json_object *jsonObjectC = json_object_array_get_idx(jsonObject, i);
        const char *str = json_object_to_json_string(jsonObjectC);
        // 检查要复制的字符串长度是否超过目标数组长度
        if (strlen(str) >= len) {
            return -1;
        }
        strncpy(arrayRet[i], str, len);
    }
    json_object_put(jsonObject);
    return size;
}
