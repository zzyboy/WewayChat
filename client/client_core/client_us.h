/*
 *客户端的头文件，用于实现功能
 * */

#ifndef _CLIENT_US_H
#define _CLIENT_US_H

#include <stdio.h>
#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <pthread.h>
#include <locale.h>
#include <string.h>
#include <stdlib.h>
//#include <ncurses.h>
#include "../../include/server.h"
#include "../../include/json_chat.h"
#include "../../include/user.h"



/*
 *初始化
 *参数:	daunko:端口
 *	ip: IP地址 
 *返回值：套接字
 * */
int regis_init(int duanko, char *ip);


/*
 *注册账号
 *参数: sockfd:套接字
 *	username:用户名
 *	passeord:密码
 *返回值：成功：0； 失败：-1；
 * */
int register_account(int sockfd, char *username, char *password);



/*
 *登录账号
 *参数: sockfd:套接字
 *      username:用户名
 *      passeord:密码
 *返回值：成功：用户ID； 失败：-1；
 * */
int log_in(int sockfd, char *username, char *password);


/*
 *搜索用户
 *参数:	sockfd:套接字
 *		username:添加的好友名
 *返回值：成功返回好友ID；失败：-1
 * */
int seek_us(int sockfd, char *username);


/*
 *查看所有好友
 *参数：sockfd:套接字
 *		user_id：用户ID
 *		fds：用户结构体数组(user_t)指针的指针
 *注意：1.不需要对user_t赋空间
 *		2.使用后要对user_t释放空间(free)	
 *返回值：成功：user_t数组的长度；失败：-1
 * */
int seeks_us(int sockfd, int user_id, user_t **frs);


/*
 *添加好友
 *参数: sockfd:套接字
 *		user_id:用户ID
 *		fr_id:好友id
 *返回值：成功：0； 失败：-1；
 * */
int add_us(int sockfd, int user_id, int fr_id);



/*
 *删除好友
 *参数: sockfd:套接字
 *      username:用户名
 *返回值：成功：0； 失败：-1；
 * */
int delete_us(int sockfd, int user_id,int fr_id);


/*
 *用户私聊
 *参数: sockfd:套接字
 *		user_id:用户ID
 *		fr_id:好友id
 *		username：用户名字
 *		content：传入的信息
 *返回值：成功返回0，失败返回-1
 * */
int us_private(int sockfd, int user_id, int fr_id, char *username, char *content);


/*
 *创造群组
 *参数: sockfd:套接字
 *		user_id:用户ID
 *		group_name:群名
 *		creator_name:群主名字
 *		remark:群备注
 *返回值：成功返回0，失败返回-1
 * */
int create_groups(int sockfd, int user_id, char *group_name, char *creator_name, char *remark);


/*
 *加入群组
 *参数: sockfd:套接字
 *		group_id:群ID
 *		member_id:加入的成员ID
 *返回值：成功返回0，失败返回-1
 * */
int join_group(int sockfd, int group_id, int member_id);


/*
 *用户群聊
 *参数: sockfd:套接字
 *		user_id:用户ID
 *		group_id:群ID
 *		username：用户名字
 *		content：传入的信息
 *返回值：成功返回0，失败返回-1
 * */
int us_group_chat(int sockfd, int user_id, int group_id, char *username, char *content);




/**
 * 发送信息
 * @param sockfd sokfd套接字
 * @param message 发送信息
 * @paeam buf	用于保存传过来的值
 */
void send_message(int sockfd,char *message, char *buf);


/*将传过来的数据的接包成user_t的数组
 *参数：sockfd:套接字
 *		user_id：用户ID
 *		fds：用户结构体数组(user_t)指针的指针
 *返回值：user_t数组的长度
 * */
int user_group(char *p, user_t **frs);

#endif
