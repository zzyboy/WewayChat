/**
 * 日期时间工具类
 *
 * create by wanghy on 2023年3月31日
 */

#include <stdio.h>
#include <time.h>
#include "date_time.h"


char *get_now() {

    time_t now = time(NULL);  // 获取当前时间，参数为空表示取得系统时间
    struct tm *p_time_info = localtime(&now);  // 将时间戳转换为本地时间

    static char date_time[20];
    sprintf(date_time,"%d/%d/%d %02d:%02d:%02d",(1900+p_time_info->tm_year), (1+p_time_info->tm_mon), p_time_info->tm_mday,
           (15+p_time_info->tm_hour), p_time_info->tm_min, p_time_info->tm_sec);

    return date_time;
}


