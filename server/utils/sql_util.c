/**
 * 获取自增ID
 */


int get_insert_id(char *table_name,int *insert_id)
{

  //制作句柄
    sqlite3 *ppDb;
    sqlite3_stmt *ppStmt;

    //打开操作
    int ret=sqlite3_open(DATABASE_PATH,&ppDb);
    if(ret!=SQLITE_OK){
            perror("sqlite_open error");
            return -1;
    }

    printf("已打开数据库\n");

    //查询用户信息
    char sql[100] = "select last_insert_rowid() from ? LIMIT 1;";
    ret = sqlite3_prepare_v2(ppDb,sql,-1,&ppStmt,NULL);
    if(ret != SQLITE_OK) {
        perror("sqlite_prepare");
        return -1;
    }

    char table[30];
    strcpy(table,table_name);
    sqlite3_bind_text(ppStmt, 1, table,strlen(table),NULL);

    bool flag = false;
    int count = 0;
    while(sqlite3_step(ppStmt) == SQLITE_ROW) {
        insert_id = sqlite3_column_int(ppStmt,0);
        flag = true;
        count++;
    }

    if(!flag) {
        sqlite3_finalize(ppStmt);
        sqlite3_close(ppDb);
        return -1;
    }


    sqlite3_finalize(ppStmt);
    sqlite3_close(ppDb);
    return 0;

}


