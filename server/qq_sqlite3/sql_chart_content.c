
/**
 * qq聊天室项目sql头文件
 * create by wanghy on 2023年3月29
 */
#include "sql_chart_content.h"
#include <sqlite3.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "../utils/date_time.h"

#define DEBUG printf("%s %d \n",__FILE_NAME__,__LINE__);
#define DATABASE_PATH "/home/user/shared/chart.db"


/**
 * 添加聊天记录
 * 入参 content_t 聊天记录结构体
 * 出参 0:成功 -1:失败
 */
int add_chart_content(content_t *content)
{

    //制作句柄
    sqlite3 *ppDb;
    sqlite3_stmt *ppStmt;

    //打开操作
    int ret=sqlite3_open(DATABASE_PATH,&ppDb);
    if(ret!=SQLITE_OK){
            perror("sqlite_open error");
            return -1;
    }

    printf("已打开数据库\n");

    //添加聊天记录
    char sql[200] = "insert into t_chart_content(sender_id,sender_name,receiver_id,receiver_name,content,type,create_time) values(?,?,?,?,?,?,?);";
    ret = sqlite3_prepare_v2(ppDb,sql,-1,&ppStmt,NULL);
    if(ret != SQLITE_OK) {
        perror("sqlite_prepare");
        return -1;
    }

    sqlite3_bind_int(ppStmt, 1, content->sender_id);
    sqlite3_bind_text(ppStmt, 2, content->sender_name,strlen(content->sender_name),NULL);
    sqlite3_bind_int(ppStmt, 3, content->receiver_id);
    sqlite3_bind_text(ppStmt, 4, content->receiver_name,strlen(content->receiver_name),NULL);
    sqlite3_bind_text(ppStmt, 5, content->content,strlen(content->content),NULL);
    sqlite3_bind_int(ppStmt, 6, content->type);
    char time[50];
    strcpy(time,get_now());
    sqlite3_bind_text(ppStmt,7, time, strlen(time),NULL);
//    char date[20];
//    strcpy(date,get_now());
//    sqlite3_bind_text(ppStmt, 7,date,strlen(date),NULL);

    ret = sqlite3_step(ppStmt);
    if(ret != SQLITE_DONE){
        perror("sqlite3_step");
        sqlite3_finalize(ppStmt);
        sqlite3_close(ppDb);
        return -1;
    }

    sqlite3_finalize(ppStmt);
    sqlite3_close(ppDb);

    return 0;
}


/**
 * 获取聊天记录
 * 入参 sender_id 发送者ID 必传
 *	receiver_id 接受者ID 必传
 * 出参 0:成功 
 *	-1:失败
 */
int get_chart_content_by_sender_id_and_receiver_id(int sender_id,int receiver_id,content_t *ts)
{
     //制作句柄
    sqlite3 *ppDb;
    sqlite3_stmt *ppStmt;

    //打开操作
    int ret=sqlite3_open(DATABASE_PATH,&ppDb);
    if(ret!=SQLITE_OK){
            perror("sqlite_open error");
            return -1;
    }

    printf("已打开数据库\n");

    //获取聊天记录
    char sql[100] = "select * from t_chart_content where sender_id in(?,?) and receiver_id in(?,?);";
    ret = sqlite3_prepare_v2(ppDb,sql,-1,&ppStmt,NULL);
    if(ret != SQLITE_OK) {
        perror("sqlite_prepare");
        return -1;
    }

    sqlite3_bind_int(ppStmt, 1,sender_id);
    sqlite3_bind_int(ppStmt, 2,receiver_id);
    sqlite3_bind_int(ppStmt, 3,sender_id);
    sqlite3_bind_int(ppStmt, 4,receiver_id);

    int j = 0;
    bool flag = false;
    while(sqlite3_step(ppStmt) == SQLITE_ROW) {
        content_t cont;
        cont.id = sqlite3_column_int(ppStmt,0);
        cont.sender_id = sqlite3_column_int(ppStmt,1);
        strcpy(cont.sender_name,sqlite3_column_text(ppStmt,2));
        cont.receiver_id = sqlite3_column_int(ppStmt,3);
        strcpy(cont.receiver_name,sqlite3_column_text(ppStmt,4));
        strcpy(cont.content,sqlite3_column_text(ppStmt,5));
        cont.type = sqlite3_column_int(ppStmt,6);
        strcpy(cont.create_time,sqlite3_column_text(ppStmt,7));

        ts[j++] = cont;
        flag = true;
    }

    if(!flag) {
        sqlite3_finalize(ppStmt);
        sqlite3_close(ppDb);
        return -1;
    }

    sqlite3_finalize(ppStmt);
    sqlite3_close(ppDb);

    return j;
}
