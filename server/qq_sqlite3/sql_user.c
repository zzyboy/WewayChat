/**
 *
 * qq聊天室项目sql
 *
 * create by wanghy on 2023年3月29
 */
#include "../../include/user.h"
#include <sqlite3.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "sql_user.h"
#include "../utils/date_time.h"
//#define DATABASE_PATH "/Users/zhouzhenyuan/CLionProjects/WeWayChat/server/db/server.db"
#define DATABASE_PATH "/home/user/shared/chart.db"

//添加用户信息
int add_user(user_t *u) {

    //制作句柄
    sqlite3 *ppDb;
    sqlite3_stmt *ppStmt;
    
    //打开操作 
    int ret=sqlite3_open(DATABASE_PATH,&ppDb);
    if(ret!=SQLITE_OK){
	    perror("sqlite_open error");
	    return -1;
    }

    printf("已打开数据库\n");
    
    //查询用户信息
    char sql[100] = "insert into t_user(user_name,passwd,create_time) values(?,?,?);";
    ret = sqlite3_prepare_v2(ppDb,sql,-1,&ppStmt,NULL);
    if(ret != SQLITE_OK) {
	perror("sqlite_prepare");
	return -1;
    }
    
    sqlite3_bind_text(ppStmt, 1, u->user_name,strlen(u->user_name),NULL);
    sqlite3_bind_text(ppStmt, 2, u->passwd,strlen(u->passwd),NULL);
    char date[20];
    strcpy(date,get_now());
    sqlite3_bind_text(ppStmt, 3, date,strlen(date),NULL);

    ret = sqlite3_step(ppStmt);
    if(ret != SQLITE_DONE){
	perror("sqlite3_step");
	sqlite3_finalize(ppStmt);
	sqlite3_close(ppDb);
	return -1;
    }

    sqlite3_finalize(ppStmt);
    sqlite3_close(ppDb);
    return 0;
}



//查询用户信息
int get_user_by_id(int user_id,user_t *u) {

    //制作句柄
    sqlite3 *ppDb;
    sqlite3_stmt *ppStmt;
    
    //打开操作 
    int ret=sqlite3_open(DATABASE_PATH,&ppDb);
    if(ret!=SQLITE_OK){
	    perror("sqlite_open error");
	    return -1;
    }

    printf("已打开数据库\n");
    
    //查询用户信息
    char sql[100] = "select * from t_user where user_id = ?;";
    ret = sqlite3_prepare_v2(ppDb,sql,-1,&ppStmt,NULL);
    if(ret != SQLITE_OK) {
	perror("sqlite_prepare");
	return -1;
    }
    
    sqlite3_bind_int(ppStmt, 1,user_id);

    bool flag = false;
    int count = 0;
    while(sqlite3_step(ppStmt) == SQLITE_ROW) {
	u->user_id = sqlite3_column_int(ppStmt,0);
	strcpy(u->user_name,sqlite3_column_text(ppStmt,1));
	strcpy(u->passwd,sqlite3_column_text(ppStmt,2));
    strcpy(u->create_time, sqlite3_column_text(ppStmt,3));

	flag = true;
	count++;
    }
    
    if(!flag) {
	sqlite3_finalize(ppStmt);
	sqlite3_close(ppDb);
	return -1;
    }

    sqlite3_finalize(ppStmt);
    sqlite3_close(ppDb);
    return count;
}


//通过用户名查询用户信息
int get_user_by_user_name(char user_name[50],user_t *u) {

    //制作句柄
    sqlite3 *ppDb;
    sqlite3_stmt *ppStmt;
    //打开操作 
    int ret=sqlite3_open(DATABASE_PATH,&ppDb);
    if(ret!=SQLITE_OK){
	    perror("sqlite_open error");
	    return -1;
    }

    printf("已打开数据库\n");
    
    //查询用户信息
    char sql[100] = "select * from t_user where user_name = ?;";
    ret = sqlite3_prepare_v2(ppDb,sql,-1,&ppStmt,NULL);
    if(ret != SQLITE_OK) {
	perror("sqlite_prepare");
	return -1;
    }
    
    sqlite3_bind_text(ppStmt, 1,user_name,strlen(user_name),NULL);
    
    bool flag = false;
    int count = 0;
    while(sqlite3_step(ppStmt) == SQLITE_ROW) {
	u->user_id = sqlite3_column_int(ppStmt,0);
	strcpy(u->user_name,sqlite3_column_text(ppStmt,1));
	strcpy(u->passwd,sqlite3_column_text(ppStmt,2));
    strcpy(u->create_time, sqlite3_column_text(ppStmt,3));


	flag = true;
	count++;
    }

    if(!flag) {
	sqlite3_finalize(ppStmt);
	sqlite3_close(ppDb);
	return -1;
    }

    sqlite3_finalize(ppStmt);
    sqlite3_close(ppDb);
    return count;
}


//通过用户名和密码查询用户信息
int get_user_by_user_name_and_passwd(char user_name[50],char passwd[50],user_t *u) {

    //制作句柄
    sqlite3 *ppDb;
    sqlite3_stmt *ppStmt;

    //打开操作 
    int ret=sqlite3_open(DATABASE_PATH,&ppDb);
    if(ret!=SQLITE_OK){
	    perror("sqlite_open error");
	    return -1;
    }

    printf("已打开数据库\n");
    printf("%s , %s 断点1",user_name,passwd);
    //查询用户信息
    char sql[100] = "select * from t_user where user_name = ? and passwd = ?;";
    ret = sqlite3_prepare_v2(ppDb,sql,-1,&ppStmt,NULL);
    if(ret != SQLITE_OK) {
	perror("sqlite_prepare");
	return -1;
    }
    
    sqlite3_bind_text(ppStmt, 1,user_name,strlen(user_name),NULL);
    sqlite3_bind_text(ppStmt, 2,passwd,strlen(passwd),NULL);

    printf("%s , %s 断点2",user_name,passwd);
    bool flag = false;
    int count = 0;
    while(sqlite3_step(ppStmt) == SQLITE_ROW) {
	u->user_id = sqlite3_column_int(ppStmt,0);
	strcpy(u->user_name,(const char *)sqlite3_column_text(ppStmt,1));
	strcpy(u->passwd,(const char *)sqlite3_column_text(ppStmt,2));
    strcpy(u->create_time, (const char *)sqlite3_column_text(ppStmt,3));

	flag = true;
	count++;
    }

    if(!flag) {
	sqlite3_finalize(ppStmt);
	sqlite3_close(ppDb);
	return -1;
    }

    sqlite3_finalize(ppStmt);
    sqlite3_close(ppDb);
    return count;
}


//修改用户信息
int edit_user(user_t *u) {

    //制作句柄
    sqlite3 *ppDb;
    sqlite3_stmt *ppStmt;
    
    //打开操作 
    int ret=sqlite3_open(DATABASE_PATH,&ppDb);
    if(ret!=SQLITE_OK){
	    perror("sqlite_open error");
	    return -1;
    }

    printf("已打开数据库\n");
    
    //查询用户信息
    char sql[100] = "update t_user set user_name = ?,passwd = ? where user_id = ?;";
    ret = sqlite3_prepare_v2(ppDb,sql,-1,&ppStmt,NULL);
    if(ret != SQLITE_OK) {
	perror("sqlite_prepare");
	return -1;
    }
    
    sqlite3_bind_text(ppStmt, 1,u->user_name,strlen(u->user_name),NULL);
    sqlite3_bind_text(ppStmt, 2,u->passwd,strlen(u->passwd),NULL);
    sqlite3_bind_int(ppStmt, 3,u->user_id);

    ret = sqlite3_step(ppStmt);
    if(ret != SQLITE_DONE){
	perror("sqlite3_step");
	sqlite3_finalize(ppStmt);
	sqlite3_close(ppDb);
	return -1;
    }

    sqlite3_finalize(ppStmt);
    sqlite3_close(ppDb);
    return 0;
}




//删除用户信息
int del_user(int user_id) {

    //制作句柄
    sqlite3 *ppDb;
    sqlite3_stmt *ppStmt;
    
    //打开操作 
    int ret=sqlite3_open(DATABASE_PATH,&ppDb);
    if(ret!=SQLITE_OK){
	    perror("sqlite_open error");
	    return -1;
    }

    printf("已打开数据库\n");
    
    //查询用户信息
    char sql[100] = "delete from t_user where user_id = ?;";
    ret = sqlite3_prepare_v2(ppDb,sql,-1,&ppStmt,NULL);
    if(ret != SQLITE_OK) {
	perror("sqlite_prepare");
	return -1;
    }
    
    sqlite3_bind_int(ppStmt, 1,user_id);

    ret = sqlite3_step(ppStmt);
    if(ret != SQLITE_DONE){
	perror("sqlite3_step");
	sqlite3_finalize(ppStmt);
	sqlite3_close(ppDb);
	return -1;
    }

    sqlite3_finalize(ppStmt);
    sqlite3_close(ppDb);
    return 0;
}
