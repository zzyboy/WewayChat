
/**
 * 用户好友头api
 * create by wanghy on 2023年3月30
 */
#include "sql_user_friend.h"
#include <stdbool.h>

#define DATABASE_PATH "/home/user/shared/chart.db"


/**
 * 添加好友
 * 入参 user 用户结构体
 * 出参 0:成功 -1:失败
 */
int add_user_friend(int user_id,int friend_id)
{
   
    //制作句柄
    sqlite3 *ppDb;
    sqlite3_stmt *ppStmt;

    //打开操作
    int ret=sqlite3_open(DATABASE_PATH,&ppDb);
    if(ret!=SQLITE_OK){
            perror("sqlite_open error");
            return -1;
    }

    printf("已打开数据库\n");

    //添加好友
    char sql[100] = "insert into t_user_friend(user_id,friend_id) values(?,?);";
    ret = sqlite3_prepare_v2(ppDb,sql,-1,&ppStmt,NULL);
    if(ret != SQLITE_OK) {
        perror("sqlite_prepare");
        return -1;
    }

    sqlite3_bind_int(ppStmt, 1, user_id);
    sqlite3_bind_int(ppStmt, 2, friend_id);
    printf("断电7\n");
    ret = sqlite3_step(ppStmt);
    if(ret != SQLITE_DONE){
        perror("sqlite3_step");
        sqlite3_finalize(ppStmt);
        sqlite3_close(ppDb);
        return -1;
    }

    sqlite3_finalize(ppStmt);
    sqlite3_close(ppDb);
    printf("断电8\n");
    return 0;
}


/**
 * 删除好友
 * 入参 friend_id 用户好友ID 必传
 * 出参 0:成功 -1:失败
 */
int del_user_friend(int user_id,int friend_id)
{

    //制作句柄
    sqlite3 *ppDb;
    sqlite3_stmt *ppStmt;

    //打开操作
    int ret=sqlite3_open(DATABASE_PATH,&ppDb);
    if(ret!=SQLITE_OK){
            perror("sqlite_open error");
            return -1;
    }

    printf("已打开数据库\n");

    //查询用户信息
    char sql[100] = "delete from t_user_friend where user_id = ? and friend_id = ?;";
    ret = sqlite3_prepare_v2(ppDb,sql,-1,&ppStmt,NULL);
    if(ret != SQLITE_OK) {
        perror("sqlite_prepare");
        return -1;
    }

    sqlite3_bind_int(ppStmt, 1,user_id);
    sqlite3_bind_int(ppStmt, 2,friend_id);

    ret = sqlite3_step(ppStmt);
    if(ret != SQLITE_DONE){
        perror("sqlite3_step");
        sqlite3_finalize(ppStmt);
        sqlite3_close(ppDb);
        return -1;
    }

    sqlite3_finalize(ppStmt);
    sqlite3_close(ppDb);
    return 0;

}


/**
 * 获取好友
 * 入参 user_id 用户ID 必传
 * 出参 0:成功 -1:失败
 */
int get_user_friend_by_id(int user_id,user_t *friends)
{
     //制作句柄
    sqlite3 *ppDb;
    sqlite3_stmt *ppStmt;

    //打开操作
    int ret=sqlite3_open(DATABASE_PATH,&ppDb);
    if(ret!=SQLITE_OK){
            perror("sqlite_open error");
            return -1;
    }

    printf("已打开数据库\n");

    //获取好友信息
    char sql[200] = "select u.user_id,u.user_name from t_user_friend uf left join t_user u on uf.friend_id = u.user_id where uf.user_id = ?;";
    ret = sqlite3_prepare_v2(ppDb,sql,-1,&ppStmt,NULL);
    if(ret != SQLITE_OK) {
        perror("sqlite_prepare");
        return -1;
    }

    sqlite3_bind_int(ppStmt, 1,user_id);

    bool flag = false;
    int j = 0;
    while(sqlite3_step(ppStmt) == SQLITE_ROW) {
	user_t u;
	u.user_id = sqlite3_column_int(ppStmt,0);
	strcpy(u.user_name,(const char *)sqlite3_column_text(ppStmt,1));

        friends[j++] = u;

        flag = true;
    }

    if(!flag) {
        sqlite3_finalize(ppStmt);
        sqlite3_close(ppDb);
        return -1;
    }

    sqlite3_finalize(ppStmt);
    sqlite3_close(ppDb);
    return j;
}
