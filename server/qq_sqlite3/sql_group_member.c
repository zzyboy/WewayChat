
/**
 * qq聊天室项目sql头文件
 * 
 * create by wanghy on 2023年3月29
 */
#include "sql_group_member.h"
#include <stdbool.h>
#include <stdio.h>
#include <sqlite3.h>
#define DATABASE_PATH "/Users/zhouzhenyuan/CLionProjects/WeWayChat/server/db/server.db"
//#define DATABASE_PATH "/home/user/shared/chart.db" 



/**
 * 添加小组成员信息
 * 入参 gmember_t 小组成员结构体
 * 出参 0:成功 -1:失败
 */
int add_group_member(gmember_t *gm) 
{
    //制作句柄
    sqlite3 *ppDb;
    sqlite3_stmt *ppStmt;

    //打开操作
    int ret=sqlite3_open(DATABASE_PATH,&ppDb);
    if(ret!=SQLITE_OK){
            perror("sqlite_open error");
            return -1;
    }

    printf("已打开数据库\n");

    //添加小组成员信息
    char sql[100] = "insert into t_group_member(group_id,group_member_id) values(?,?);";
    ret = sqlite3_prepare_v2(ppDb,sql,-1,&ppStmt,NULL);
    if(ret != SQLITE_OK) {
        perror("sqlite_prepare");
        return -1;
    }

    sqlite3_bind_int(ppStmt, 1, gm->group_id);
    sqlite3_bind_int(ppStmt, 2, gm->group_member_id);

    ret = sqlite3_step(ppStmt);
    if(ret != SQLITE_DONE){
        perror("sqlite3_step");
        sqlite3_finalize(ppStmt);
        sqlite3_close(ppDb);
        return -1;
    }

    sqlite3_finalize(ppStmt);
    sqlite3_close(ppDb);

    return 0;
}


/**
 * 获取小组成员信息
 * 入参 group_id 小组成员ID 必传
 * 出参 0:成功 gms:小组成员ID列表
 *      -1:失败
 */
int get_group_member(int group_id,int *gms)
{
    //制作句柄
    sqlite3 *ppDb;
    sqlite3_stmt *ppStmt;

    //打开操作
    int ret=sqlite3_open(DATABASE_PATH,&ppDb);
    if(ret!=SQLITE_OK){
            perror("sqlite_open error");
            return -1;
    }

    printf("已打开数据库\n");

    //获取小组成员信息
    char sql[100] = "select * from t_group_member where group_id = ?;";
    ret = sqlite3_prepare_v2(ppDb,sql,-1,&ppStmt,NULL);
    if(ret != SQLITE_OK) {
        perror("sqlite_prepare");
        return -1;
    }

    sqlite3_bind_int(ppStmt, 1, group_id);
    
    bool flag = false;
    int j = 0;
    while(sqlite3_step(ppStmt) == SQLITE_ROW) {
        gms[j++] = sqlite3_column_int(ppStmt,1);
    
	flag = true;
    }
    
    if(!flag) {
	sqlite3_finalize(ppStmt);
	sqlite3_close(ppDb);
	return -1;
    }

    sqlite3_finalize(ppStmt);
    sqlite3_close(ppDb);
    return j;

}


/**
 * 删除小组成员信息
 * 入参 group_id 小组ID 必传
 * 出参 0:成功 -1:失败
 */
int del_group_member(int group_id,int group_member_id)
{
     //制作句柄
    sqlite3 *ppDb;
    sqlite3_stmt *ppStmt;

    //打开操作
    int ret=sqlite3_open(DATABASE_PATH,&ppDb);
    if(ret!=SQLITE_OK){
            perror("sqlite_open error");
            return -1;
    }

    printf("已打开数据库\n");

    //删小组成员信息
    char sql[100] = "delete from t_group_member where group_id = ? and group_member_id =?;";
    ret = sqlite3_prepare_v2(ppDb,sql,-1,&ppStmt,NULL);
    if(ret != SQLITE_OK) {
        perror("sqlite_prepare");
        return -1;
    }

    sqlite3_bind_int(ppStmt, 1, group_id);
    sqlite3_bind_int(ppStmt, 2, group_member_id);

    ret = sqlite3_step(ppStmt);
    if(ret != SQLITE_DONE){
        perror("sqlite3_step");
        sqlite3_finalize(ppStmt);
        sqlite3_close(ppDb);
        return -1;
    }

    sqlite3_finalize(ppStmt);
    sqlite3_close(ppDb);
    return 0;
}

