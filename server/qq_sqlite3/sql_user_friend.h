
/**
 * 用户好友头文件
 * create by wanghy on 2023年3月30
 */

#ifndef _SQL_USER_FRIEND_H_
#define	_SQL_USER_FRIEND_H_ 
#include "../../include/user.h"



typedef struct user_friend {
    
    int user_id;
    int user_friend;
} friend_t;

/**
 * 添加好友
 * 入参 user 用户结构体
 * 出参 0:成功 -1:失败
 */
int add_user_friend(int user_id,int friend_id);


/**
 * 获取好友
 * 入参 user_id 用户ID 必传
 * 出参 0:成功 -1:失败
 */
int get_user_friend_by_id(int user_id,user_t *friends);


/**
 * 删除好友
 * 入参 friend_id 用户好友ID 必传
 * 出参 0:成功 -1:失败
 */
int del_user_friend(int user_id,int friend_id);




#endif
