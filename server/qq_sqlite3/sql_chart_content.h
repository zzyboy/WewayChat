
/**
 * qq聊天室项目sql头文件
 * create by wanghy on 2023年3月29
 */
#ifndef _SQL_CHART_CONTENT_H_
#define _SQL_CHART_CONTENT_H_

typedef struct chart_content {
    
    int id;
    int sender_id;
    char sender_name[50];
    int receiver_id;
    char receiver_name[50];
    char content[1024];
    int type;   //0:私聊 1:群聊
    int group_id;//群组ID
    char create_time[20];
    
} content_t;


/**
 * 添加聊天记录
 * 入参 content_t 聊天记录结构体
 * 出参 0:成功 -1:失败
 */
int add_chart_content(content_t *content);


/**
 * 获取聊天记录
 * 入参 sender_id 发送者ID 必传
 *	receiver_id 接受者ID 必传
 * 出参 0:成功 默认获取最近10条
 *	-1:失败
 */
int get_chart_content_by_sender_id_and_receiver_id(int sender_id,int receiver_id,content_t *ts);



#endif
