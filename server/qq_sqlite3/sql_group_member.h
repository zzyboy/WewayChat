
/**
 * qq聊天室项目sql头文件
 * create by wanghy on 2023年3月29
 */

#ifndef _SQL_GROUP_MEMBER_H_
#define  _SQL_GROUP_MEMBER_H_

typedef struct group_member {
    int group_id;
    int group_member_id;
} gmember_t;


/**
 * 添加小组成员信息
 * 入参 gmember_t 小组成员结构体
 * 出参 0:成功 -1:失败
 */
int add_group_member(gmember_t *gm);


/**
 * 获取小组成员信息
 * 入参 group_id 小组成员ID 必传
 * 出参 0:成功 gms:小组成员ID列表
 *      -1:失败
 */
int get_group_member(int group_id,int *gms);


/**
 * 删除小组成员信息
 * 入参 group_id 小组ID 必传
 * 出参 0:成功 -1:失败
 */
int del_group_member(int group_id,int group_member_id);



#endif
