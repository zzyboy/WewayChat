
/**
 * qq聊天室项目sql头文件
 * create by wanghy on 2023年3月29
 */

#ifndef QQ_SQLITE_H
#define	QQ_SQLITE_H 
#include "../../include/user.h"


/**
 * 添加用户信息
 * 入参 user 用户结构体
 * 出参 0:成功 -1:失败
 */
int add_user(user_t *user);


/**
 * 修改用户信息
 * 入参 user 用户结构体 其中user_id必传
 * 出参 0:成功 -1:失败
 */
int edit_user(user_t* user);


/**
 * 获取用户信息
 * 入参 user_id 用户ID 必传
 * 出参 0:成功 -1:失败
 */
int get_user_by_id(int user_id,user_t *u);


/**
 * 获取用户信息
 * 入参 user_name  用户ID 必传
 *	passwd	   用户密码 必传
 * 出参 0:成功 -1:失败
 */
int get_user_by_user_name_and_passwd(char user_name[50],char passwd[50],user_t *u);

/**
 * 获取用户信息
 * 入参 user_name  用户名 必传
 * 出参 0:成功 -1:失败
 */
int get_user_by_user_name(char user_name[50],user_t *u);


/**
 * 删除用户信息
 * 入参 user_id 用户ID 必传
 * 出参 0:成功 -1:失败
 */
int del_user(int user_id);




#endif
