//
// Created by 周圳元 on 2023/3/29.
//
#include "../../include/user.h"
#include "../../include/json_chat.h"

client_t clients[MAX_CLIENTS];

int num_clients = 0;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

// 给客户端发送数据
void broadcast(char *message, int sender) {
   // pthread_mutex_lock(&mutex);
    printf("发送数据 -> sender: %d \n",sender);
    printf("message:%s \n",message);
    int ret = send(sender, message, strlen(message), 0);
    printf("发送结果 %d \n",ret);
   // pthread_mutex_unlock(&mutex);
}

void *handle_client(void *arg) {
    int num_t = num_clients;
    num_clients++;
    int client_socket = *(int *)arg;
    char buffer[BUFFER_SIZE];
    while (1) {
        bzero(buffer,BUFFER_SIZE);
        printf("socket :%d \n",client_socket);
        int num_bytes = recv(client_socket, buffer, BUFFER_SIZE, 0);
       // printf("%d buffer:%s \n",num_bytes,buffer);
        if (num_bytes <= 0) {
            printf("客户端退出");
            break;
        }
        int action= json_get_int(buffer,"action");
        char *data = json_get_string(buffer,"data");
        char *text = json_get_string(buffer,"text");
//        printf("%d buffer:%s \n",num_bytes,buffer);
//        buffer[num_bytes] = '\0';
        printf("data :%s actions: %d text: %s\n",data,action,text);
        char message[JSON_SIZE_MAX];
        pthread_mutex_lock(&mutex);
        user_handle(action,
                    data,
                    message,clients,num_t,num_clients);
        pthread_mutex_unlock(&mutex);
        if (json_get_int(message,"action") == USER_SEND_MESSAGE_USER) {

        } else {
            broadcast(message, client_socket);
        }
    }
    close(client_socket);
    printf("退出socket \n");
//    pthread_exit(NULL);
}

int core(int port,const char *ip_addr) {
    int server_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (server_socket == -1) {
        perror("Error creating socket\n");
        return 0;
    }

    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = INADDR_ANY;
    server_address.sin_port = htons(port);

    if (bind(server_socket, (struct sockaddr *)&server_address, sizeof(server_address)) < 0) {
        perror("Error binding socket\n");
        return 0;
    }

    listen(server_socket, MAX_CLIENTS);

    return server_socket;
}

int socket_bind(int port,const char *ip_addr) {

    int sockfd = core(port,ip_addr);

    if (sockfd == 0) {
        return 0;
    }

    while (1) {
        struct sockaddr_in client_address;
        int client_size = sizeof(client_address);
        // 拿到客户端
        int client_socket = accept(sockfd, (struct sockaddr *)&client_address, &client_size);

        if (client_socket < 0) {
            printf("Error accepting client connection\n");
            continue;
        }
        if (num_clients == MAX_CLIENTS) {
            printf("Maximum number of clients reached\n");
            close(client_socket);
            continue;
        }

        // 将客户端socket存放起来
        clients[num_clients].client = client_socket;
        printf("New client connected\n");
        pthread_t thread_id;
        pthread_attr_t pthreadAttr;
        pthread_attr_init(&pthreadAttr);
        pthread_attr_setdetachstate(&pthreadAttr,PTHREAD_CREATE_DETACHED);

        if (pthread_create(&thread_id, &pthreadAttr, handle_client, &client_socket) != 0) {
            printf("Error creating client thread\n");
            continue;
        }
    }

    return 0;
}


